// added ex post facto by /bin/generate_dart.sh
// see: https://github.com/dart-lang/protobuf/issues/368
// see: https://github.com/dart-lang/protobuf/issues/369
// ignore_for_file: unnecessary_const,annotate_overrides,sort_unnamed_constructors_first,directives_ordering

///
//  Generated code. Do not modify.
//  source: stan/protocol.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

// ignore_for_file: UNDEFINED_SHOWN_NAME,UNUSED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class StartPosition extends $pb.ProtobufEnum {
  static const StartPosition NewOnly = StartPosition._(0, 'NewOnly');
  static const StartPosition LastReceived = StartPosition._(1, 'LastReceived');
  static const StartPosition TimeDeltaStart =
      StartPosition._(2, 'TimeDeltaStart');
  static const StartPosition SequenceStart =
      StartPosition._(3, 'SequenceStart');
  static const StartPosition First = StartPosition._(4, 'First');

  static const $core.List<StartPosition> values = <StartPosition>[
    NewOnly,
    LastReceived,
    TimeDeltaStart,
    SequenceStart,
    First,
  ];

  static final $core.Map<$core.int, StartPosition> _byValue =
      $pb.ProtobufEnum.initByValue(values);
  static StartPosition valueOf($core.int value) => _byValue[value];

  const StartPosition._($core.int v, $core.String n) : super(v, n);
}
