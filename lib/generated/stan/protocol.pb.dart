// added ex post facto by /bin/generate_dart.sh
// see: https://github.com/dart-lang/protobuf/issues/368
// see: https://github.com/dart-lang/protobuf/issues/369
// ignore_for_file: unnecessary_const,annotate_overrides,sort_unnamed_constructors_first,directives_ordering

///
//  Generated code. Do not modify.
//  source: stan/protocol.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'protocol.pbenum.dart';

export 'protocol.pbenum.dart';

class PubMsg extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('PubMsg',
      package: const $pb.PackageName('pb'), createEmptyInstance: create)
    ..aOS(1, 'clientID', protoName: 'clientID')
    ..aOS(2, 'guid')
    ..aOS(3, 'subject')
    ..aOS(4, 'reply')
    ..a<$core.List<$core.int>>(5, 'data', $pb.PbFieldType.OY)
    ..a<$core.List<$core.int>>(6, 'connID', $pb.PbFieldType.OY,
        protoName: 'connID')
    ..a<$core.List<$core.int>>(10, 'sha256', $pb.PbFieldType.OY)
    ..hasRequiredFields = false;

  PubMsg._() : super();
  factory PubMsg() => create();
  factory PubMsg.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory PubMsg.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  PubMsg clone() => PubMsg()..mergeFromMessage(this);
  PubMsg copyWith(void Function(PubMsg) updates) =>
      super.copyWith((message) => updates(message as PubMsg));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PubMsg create() => PubMsg._();
  PubMsg createEmptyInstance() => create();
  static $pb.PbList<PubMsg> createRepeated() => $pb.PbList<PubMsg>();
  @$core.pragma('dart2js:noInline')
  static PubMsg getDefault() =>
      _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PubMsg>(create);
  static PubMsg _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get clientID => $_getSZ(0);
  @$pb.TagNumber(1)
  set clientID($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasClientID() => $_has(0);
  @$pb.TagNumber(1)
  void clearClientID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get guid => $_getSZ(1);
  @$pb.TagNumber(2)
  set guid($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasGuid() => $_has(1);
  @$pb.TagNumber(2)
  void clearGuid() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get subject => $_getSZ(2);
  @$pb.TagNumber(3)
  set subject($core.String v) {
    $_setString(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasSubject() => $_has(2);
  @$pb.TagNumber(3)
  void clearSubject() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get reply => $_getSZ(3);
  @$pb.TagNumber(4)
  set reply($core.String v) {
    $_setString(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasReply() => $_has(3);
  @$pb.TagNumber(4)
  void clearReply() => clearField(4);

  @$pb.TagNumber(5)
  $core.List<$core.int> get data => $_getN(4);
  @$pb.TagNumber(5)
  set data($core.List<$core.int> v) {
    $_setBytes(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasData() => $_has(4);
  @$pb.TagNumber(5)
  void clearData() => clearField(5);

  @$pb.TagNumber(6)
  $core.List<$core.int> get connID => $_getN(5);
  @$pb.TagNumber(6)
  set connID($core.List<$core.int> v) {
    $_setBytes(5, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasConnID() => $_has(5);
  @$pb.TagNumber(6)
  void clearConnID() => clearField(6);

  @$pb.TagNumber(10)
  $core.List<$core.int> get sha256 => $_getN(6);
  @$pb.TagNumber(10)
  set sha256($core.List<$core.int> v) {
    $_setBytes(6, v);
  }

  @$pb.TagNumber(10)
  $core.bool hasSha256() => $_has(6);
  @$pb.TagNumber(10)
  void clearSha256() => clearField(10);
}

class PubAck extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('PubAck',
      package: const $pb.PackageName('pb'), createEmptyInstance: create)
    ..aOS(1, 'guid')
    ..aOS(2, 'error')
    ..hasRequiredFields = false;

  PubAck._() : super();
  factory PubAck() => create();
  factory PubAck.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory PubAck.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  PubAck clone() => PubAck()..mergeFromMessage(this);
  PubAck copyWith(void Function(PubAck) updates) =>
      super.copyWith((message) => updates(message as PubAck));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PubAck create() => PubAck._();
  PubAck createEmptyInstance() => create();
  static $pb.PbList<PubAck> createRepeated() => $pb.PbList<PubAck>();
  @$core.pragma('dart2js:noInline')
  static PubAck getDefault() =>
      _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PubAck>(create);
  static PubAck _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get guid => $_getSZ(0);
  @$pb.TagNumber(1)
  set guid($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasGuid() => $_has(0);
  @$pb.TagNumber(1)
  void clearGuid() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get error => $_getSZ(1);
  @$pb.TagNumber(2)
  set error($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasError() => $_has(1);
  @$pb.TagNumber(2)
  void clearError() => clearField(2);
}

class MsgProto extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('MsgProto',
      package: const $pb.PackageName('pb'), createEmptyInstance: create)
    ..a<$fixnum.Int64>(1, 'sequence', $pb.PbFieldType.OU6,
        defaultOrMaker: $fixnum.Int64.ZERO)
    ..aOS(2, 'subject')
    ..aOS(3, 'reply')
    ..a<$core.List<$core.int>>(4, 'data', $pb.PbFieldType.OY)
    ..aInt64(5, 'timestamp')
    ..aOB(6, 'redelivered')
    ..a<$core.int>(7, 'redeliveryCount', $pb.PbFieldType.OU3,
        protoName: 'redeliveryCount')
    ..a<$core.int>(10, 'CRC32', $pb.PbFieldType.OU3, protoName: 'CRC32')
    ..hasRequiredFields = false;

  MsgProto._() : super();
  factory MsgProto() => create();
  factory MsgProto.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory MsgProto.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  MsgProto clone() => MsgProto()..mergeFromMessage(this);
  MsgProto copyWith(void Function(MsgProto) updates) =>
      super.copyWith((message) => updates(message as MsgProto));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MsgProto create() => MsgProto._();
  MsgProto createEmptyInstance() => create();
  static $pb.PbList<MsgProto> createRepeated() => $pb.PbList<MsgProto>();
  @$core.pragma('dart2js:noInline')
  static MsgProto getDefault() =>
      _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MsgProto>(create);
  static MsgProto _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get sequence => $_getI64(0);
  @$pb.TagNumber(1)
  set sequence($fixnum.Int64 v) {
    $_setInt64(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasSequence() => $_has(0);
  @$pb.TagNumber(1)
  void clearSequence() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get subject => $_getSZ(1);
  @$pb.TagNumber(2)
  set subject($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasSubject() => $_has(1);
  @$pb.TagNumber(2)
  void clearSubject() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get reply => $_getSZ(2);
  @$pb.TagNumber(3)
  set reply($core.String v) {
    $_setString(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasReply() => $_has(2);
  @$pb.TagNumber(3)
  void clearReply() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<$core.int> get data => $_getN(3);
  @$pb.TagNumber(4)
  set data($core.List<$core.int> v) {
    $_setBytes(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasData() => $_has(3);
  @$pb.TagNumber(4)
  void clearData() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get timestamp => $_getI64(4);
  @$pb.TagNumber(5)
  set timestamp($fixnum.Int64 v) {
    $_setInt64(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasTimestamp() => $_has(4);
  @$pb.TagNumber(5)
  void clearTimestamp() => clearField(5);

  @$pb.TagNumber(6)
  $core.bool get redelivered => $_getBF(5);
  @$pb.TagNumber(6)
  set redelivered($core.bool v) {
    $_setBool(5, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasRedelivered() => $_has(5);
  @$pb.TagNumber(6)
  void clearRedelivered() => clearField(6);

  @$pb.TagNumber(7)
  $core.int get redeliveryCount => $_getIZ(6);
  @$pb.TagNumber(7)
  set redeliveryCount($core.int v) {
    $_setUnsignedInt32(6, v);
  }

  @$pb.TagNumber(7)
  $core.bool hasRedeliveryCount() => $_has(6);
  @$pb.TagNumber(7)
  void clearRedeliveryCount() => clearField(7);

  @$pb.TagNumber(10)
  $core.int get cRC32 => $_getIZ(7);
  @$pb.TagNumber(10)
  set cRC32($core.int v) {
    $_setUnsignedInt32(7, v);
  }

  @$pb.TagNumber(10)
  $core.bool hasCRC32() => $_has(7);
  @$pb.TagNumber(10)
  void clearCRC32() => clearField(10);
}

class Ack extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Ack',
      package: const $pb.PackageName('pb'), createEmptyInstance: create)
    ..aOS(1, 'subject')
    ..a<$fixnum.Int64>(2, 'sequence', $pb.PbFieldType.OU6,
        defaultOrMaker: $fixnum.Int64.ZERO)
    ..hasRequiredFields = false;

  Ack._() : super();
  factory Ack() => create();
  factory Ack.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory Ack.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  Ack clone() => Ack()..mergeFromMessage(this);
  Ack copyWith(void Function(Ack) updates) =>
      super.copyWith((message) => updates(message as Ack));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Ack create() => Ack._();
  Ack createEmptyInstance() => create();
  static $pb.PbList<Ack> createRepeated() => $pb.PbList<Ack>();
  @$core.pragma('dart2js:noInline')
  static Ack getDefault() =>
      _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Ack>(create);
  static Ack _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get subject => $_getSZ(0);
  @$pb.TagNumber(1)
  set subject($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasSubject() => $_has(0);
  @$pb.TagNumber(1)
  void clearSubject() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get sequence => $_getI64(1);
  @$pb.TagNumber(2)
  set sequence($fixnum.Int64 v) {
    $_setInt64(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasSequence() => $_has(1);
  @$pb.TagNumber(2)
  void clearSequence() => clearField(2);
}

class ConnectRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ConnectRequest',
      package: const $pb.PackageName('pb'), createEmptyInstance: create)
    ..aOS(1, 'clientID', protoName: 'clientID')
    ..aOS(2, 'heartbeatInbox', protoName: 'heartbeatInbox')
    ..a<$core.int>(3, 'protocol', $pb.PbFieldType.O3)
    ..a<$core.List<$core.int>>(4, 'connID', $pb.PbFieldType.OY,
        protoName: 'connID')
    ..a<$core.int>(5, 'pingInterval', $pb.PbFieldType.O3,
        protoName: 'pingInterval')
    ..a<$core.int>(6, 'pingMaxOut', $pb.PbFieldType.O3, protoName: 'pingMaxOut')
    ..hasRequiredFields = false;

  ConnectRequest._() : super();
  factory ConnectRequest() => create();
  factory ConnectRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory ConnectRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  ConnectRequest clone() => ConnectRequest()..mergeFromMessage(this);
  ConnectRequest copyWith(void Function(ConnectRequest) updates) =>
      super.copyWith((message) => updates(message as ConnectRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ConnectRequest create() => ConnectRequest._();
  ConnectRequest createEmptyInstance() => create();
  static $pb.PbList<ConnectRequest> createRepeated() =>
      $pb.PbList<ConnectRequest>();
  @$core.pragma('dart2js:noInline')
  static ConnectRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<ConnectRequest>(create);
  static ConnectRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get clientID => $_getSZ(0);
  @$pb.TagNumber(1)
  set clientID($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasClientID() => $_has(0);
  @$pb.TagNumber(1)
  void clearClientID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get heartbeatInbox => $_getSZ(1);
  @$pb.TagNumber(2)
  set heartbeatInbox($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasHeartbeatInbox() => $_has(1);
  @$pb.TagNumber(2)
  void clearHeartbeatInbox() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get protocol => $_getIZ(2);
  @$pb.TagNumber(3)
  set protocol($core.int v) {
    $_setSignedInt32(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasProtocol() => $_has(2);
  @$pb.TagNumber(3)
  void clearProtocol() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<$core.int> get connID => $_getN(3);
  @$pb.TagNumber(4)
  set connID($core.List<$core.int> v) {
    $_setBytes(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasConnID() => $_has(3);
  @$pb.TagNumber(4)
  void clearConnID() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get pingInterval => $_getIZ(4);
  @$pb.TagNumber(5)
  set pingInterval($core.int v) {
    $_setSignedInt32(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasPingInterval() => $_has(4);
  @$pb.TagNumber(5)
  void clearPingInterval() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get pingMaxOut => $_getIZ(5);
  @$pb.TagNumber(6)
  set pingMaxOut($core.int v) {
    $_setSignedInt32(5, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasPingMaxOut() => $_has(5);
  @$pb.TagNumber(6)
  void clearPingMaxOut() => clearField(6);
}

class ConnectResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ConnectResponse',
      package: const $pb.PackageName('pb'), createEmptyInstance: create)
    ..aOS(1, 'pubPrefix', protoName: 'pubPrefix')
    ..aOS(2, 'subRequests', protoName: 'subRequests')
    ..aOS(3, 'unsubRequests', protoName: 'unsubRequests')
    ..aOS(4, 'closeRequests', protoName: 'closeRequests')
    ..aOS(5, 'error')
    ..aOS(6, 'subCloseRequests', protoName: 'subCloseRequests')
    ..aOS(7, 'pingRequests', protoName: 'pingRequests')
    ..a<$core.int>(8, 'pingInterval', $pb.PbFieldType.O3,
        protoName: 'pingInterval')
    ..a<$core.int>(9, 'pingMaxOut', $pb.PbFieldType.O3, protoName: 'pingMaxOut')
    ..a<$core.int>(10, 'protocol', $pb.PbFieldType.O3)
    ..aOS(100, 'publicKey', protoName: 'publicKey')
    ..hasRequiredFields = false;

  ConnectResponse._() : super();
  factory ConnectResponse() => create();
  factory ConnectResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory ConnectResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  ConnectResponse clone() => ConnectResponse()..mergeFromMessage(this);
  ConnectResponse copyWith(void Function(ConnectResponse) updates) =>
      super.copyWith((message) => updates(message as ConnectResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ConnectResponse create() => ConnectResponse._();
  ConnectResponse createEmptyInstance() => create();
  static $pb.PbList<ConnectResponse> createRepeated() =>
      $pb.PbList<ConnectResponse>();
  @$core.pragma('dart2js:noInline')
  static ConnectResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<ConnectResponse>(create);
  static ConnectResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get pubPrefix => $_getSZ(0);
  @$pb.TagNumber(1)
  set pubPrefix($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasPubPrefix() => $_has(0);
  @$pb.TagNumber(1)
  void clearPubPrefix() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get subRequests => $_getSZ(1);
  @$pb.TagNumber(2)
  set subRequests($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasSubRequests() => $_has(1);
  @$pb.TagNumber(2)
  void clearSubRequests() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get unsubRequests => $_getSZ(2);
  @$pb.TagNumber(3)
  set unsubRequests($core.String v) {
    $_setString(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasUnsubRequests() => $_has(2);
  @$pb.TagNumber(3)
  void clearUnsubRequests() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get closeRequests => $_getSZ(3);
  @$pb.TagNumber(4)
  set closeRequests($core.String v) {
    $_setString(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasCloseRequests() => $_has(3);
  @$pb.TagNumber(4)
  void clearCloseRequests() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get error => $_getSZ(4);
  @$pb.TagNumber(5)
  set error($core.String v) {
    $_setString(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasError() => $_has(4);
  @$pb.TagNumber(5)
  void clearError() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get subCloseRequests => $_getSZ(5);
  @$pb.TagNumber(6)
  set subCloseRequests($core.String v) {
    $_setString(5, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasSubCloseRequests() => $_has(5);
  @$pb.TagNumber(6)
  void clearSubCloseRequests() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get pingRequests => $_getSZ(6);
  @$pb.TagNumber(7)
  set pingRequests($core.String v) {
    $_setString(6, v);
  }

  @$pb.TagNumber(7)
  $core.bool hasPingRequests() => $_has(6);
  @$pb.TagNumber(7)
  void clearPingRequests() => clearField(7);

  @$pb.TagNumber(8)
  $core.int get pingInterval => $_getIZ(7);
  @$pb.TagNumber(8)
  set pingInterval($core.int v) {
    $_setSignedInt32(7, v);
  }

  @$pb.TagNumber(8)
  $core.bool hasPingInterval() => $_has(7);
  @$pb.TagNumber(8)
  void clearPingInterval() => clearField(8);

  @$pb.TagNumber(9)
  $core.int get pingMaxOut => $_getIZ(8);
  @$pb.TagNumber(9)
  set pingMaxOut($core.int v) {
    $_setSignedInt32(8, v);
  }

  @$pb.TagNumber(9)
  $core.bool hasPingMaxOut() => $_has(8);
  @$pb.TagNumber(9)
  void clearPingMaxOut() => clearField(9);

  @$pb.TagNumber(10)
  $core.int get protocol => $_getIZ(9);
  @$pb.TagNumber(10)
  set protocol($core.int v) {
    $_setSignedInt32(9, v);
  }

  @$pb.TagNumber(10)
  $core.bool hasProtocol() => $_has(9);
  @$pb.TagNumber(10)
  void clearProtocol() => clearField(10);

  @$pb.TagNumber(100)
  $core.String get publicKey => $_getSZ(10);
  @$pb.TagNumber(100)
  set publicKey($core.String v) {
    $_setString(10, v);
  }

  @$pb.TagNumber(100)
  $core.bool hasPublicKey() => $_has(10);
  @$pb.TagNumber(100)
  void clearPublicKey() => clearField(100);
}

class Ping extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Ping',
      package: const $pb.PackageName('pb'), createEmptyInstance: create)
    ..a<$core.List<$core.int>>(1, 'connID', $pb.PbFieldType.OY,
        protoName: 'connID')
    ..hasRequiredFields = false;

  Ping._() : super();
  factory Ping() => create();
  factory Ping.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory Ping.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  Ping clone() => Ping()..mergeFromMessage(this);
  Ping copyWith(void Function(Ping) updates) =>
      super.copyWith((message) => updates(message as Ping));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Ping create() => Ping._();
  Ping createEmptyInstance() => create();
  static $pb.PbList<Ping> createRepeated() => $pb.PbList<Ping>();
  @$core.pragma('dart2js:noInline')
  static Ping getDefault() =>
      _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Ping>(create);
  static Ping _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.int> get connID => $_getN(0);
  @$pb.TagNumber(1)
  set connID($core.List<$core.int> v) {
    $_setBytes(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasConnID() => $_has(0);
  @$pb.TagNumber(1)
  void clearConnID() => clearField(1);
}

class PingResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('PingResponse',
      package: const $pb.PackageName('pb'), createEmptyInstance: create)
    ..aOS(1, 'error')
    ..hasRequiredFields = false;

  PingResponse._() : super();
  factory PingResponse() => create();
  factory PingResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory PingResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  PingResponse clone() => PingResponse()..mergeFromMessage(this);
  PingResponse copyWith(void Function(PingResponse) updates) =>
      super.copyWith((message) => updates(message as PingResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PingResponse create() => PingResponse._();
  PingResponse createEmptyInstance() => create();
  static $pb.PbList<PingResponse> createRepeated() =>
      $pb.PbList<PingResponse>();
  @$core.pragma('dart2js:noInline')
  static PingResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<PingResponse>(create);
  static PingResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get error => $_getSZ(0);
  @$pb.TagNumber(1)
  set error($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasError() => $_has(0);
  @$pb.TagNumber(1)
  void clearError() => clearField(1);
}

class SubscriptionRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SubscriptionRequest',
      package: const $pb.PackageName('pb'), createEmptyInstance: create)
    ..aOS(1, 'clientID', protoName: 'clientID')
    ..aOS(2, 'subject')
    ..aOS(3, 'qGroup', protoName: 'qGroup')
    ..aOS(4, 'inbox')
    ..a<$core.int>(5, 'maxInFlight', $pb.PbFieldType.O3,
        protoName: 'maxInFlight')
    ..a<$core.int>(6, 'ackWaitInSecs', $pb.PbFieldType.O3,
        protoName: 'ackWaitInSecs')
    ..aOS(7, 'durableName', protoName: 'durableName')
    ..e<StartPosition>(10, 'startPosition', $pb.PbFieldType.OE,
        protoName: 'startPosition',
        defaultOrMaker: StartPosition.NewOnly,
        valueOf: StartPosition.valueOf,
        enumValues: StartPosition.values)
    ..a<$fixnum.Int64>(11, 'startSequence', $pb.PbFieldType.OU6,
        protoName: 'startSequence', defaultOrMaker: $fixnum.Int64.ZERO)
    ..aInt64(12, 'startTimeDelta', protoName: 'startTimeDelta')
    ..hasRequiredFields = false;

  SubscriptionRequest._() : super();
  factory SubscriptionRequest() => create();
  factory SubscriptionRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory SubscriptionRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  SubscriptionRequest clone() => SubscriptionRequest()..mergeFromMessage(this);
  SubscriptionRequest copyWith(void Function(SubscriptionRequest) updates) =>
      super.copyWith((message) => updates(message as SubscriptionRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SubscriptionRequest create() => SubscriptionRequest._();
  SubscriptionRequest createEmptyInstance() => create();
  static $pb.PbList<SubscriptionRequest> createRepeated() =>
      $pb.PbList<SubscriptionRequest>();
  @$core.pragma('dart2js:noInline')
  static SubscriptionRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<SubscriptionRequest>(create);
  static SubscriptionRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get clientID => $_getSZ(0);
  @$pb.TagNumber(1)
  set clientID($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasClientID() => $_has(0);
  @$pb.TagNumber(1)
  void clearClientID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get subject => $_getSZ(1);
  @$pb.TagNumber(2)
  set subject($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasSubject() => $_has(1);
  @$pb.TagNumber(2)
  void clearSubject() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get qGroup => $_getSZ(2);
  @$pb.TagNumber(3)
  set qGroup($core.String v) {
    $_setString(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasQGroup() => $_has(2);
  @$pb.TagNumber(3)
  void clearQGroup() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get inbox => $_getSZ(3);
  @$pb.TagNumber(4)
  set inbox($core.String v) {
    $_setString(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasInbox() => $_has(3);
  @$pb.TagNumber(4)
  void clearInbox() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get maxInFlight => $_getIZ(4);
  @$pb.TagNumber(5)
  set maxInFlight($core.int v) {
    $_setSignedInt32(4, v);
  }

  @$pb.TagNumber(5)
  $core.bool hasMaxInFlight() => $_has(4);
  @$pb.TagNumber(5)
  void clearMaxInFlight() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get ackWaitInSecs => $_getIZ(5);
  @$pb.TagNumber(6)
  set ackWaitInSecs($core.int v) {
    $_setSignedInt32(5, v);
  }

  @$pb.TagNumber(6)
  $core.bool hasAckWaitInSecs() => $_has(5);
  @$pb.TagNumber(6)
  void clearAckWaitInSecs() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get durableName => $_getSZ(6);
  @$pb.TagNumber(7)
  set durableName($core.String v) {
    $_setString(6, v);
  }

  @$pb.TagNumber(7)
  $core.bool hasDurableName() => $_has(6);
  @$pb.TagNumber(7)
  void clearDurableName() => clearField(7);

  @$pb.TagNumber(10)
  StartPosition get startPosition => $_getN(7);
  @$pb.TagNumber(10)
  set startPosition(StartPosition v) {
    setField(10, v);
  }

  @$pb.TagNumber(10)
  $core.bool hasStartPosition() => $_has(7);
  @$pb.TagNumber(10)
  void clearStartPosition() => clearField(10);

  @$pb.TagNumber(11)
  $fixnum.Int64 get startSequence => $_getI64(8);
  @$pb.TagNumber(11)
  set startSequence($fixnum.Int64 v) {
    $_setInt64(8, v);
  }

  @$pb.TagNumber(11)
  $core.bool hasStartSequence() => $_has(8);
  @$pb.TagNumber(11)
  void clearStartSequence() => clearField(11);

  @$pb.TagNumber(12)
  $fixnum.Int64 get startTimeDelta => $_getI64(9);
  @$pb.TagNumber(12)
  set startTimeDelta($fixnum.Int64 v) {
    $_setInt64(9, v);
  }

  @$pb.TagNumber(12)
  $core.bool hasStartTimeDelta() => $_has(9);
  @$pb.TagNumber(12)
  void clearStartTimeDelta() => clearField(12);
}

class SubscriptionResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SubscriptionResponse',
      package: const $pb.PackageName('pb'), createEmptyInstance: create)
    ..aOS(2, 'ackInbox', protoName: 'ackInbox')
    ..aOS(3, 'error')
    ..hasRequiredFields = false;

  SubscriptionResponse._() : super();
  factory SubscriptionResponse() => create();
  factory SubscriptionResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory SubscriptionResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  SubscriptionResponse clone() =>
      SubscriptionResponse()..mergeFromMessage(this);
  SubscriptionResponse copyWith(void Function(SubscriptionResponse) updates) =>
      super.copyWith((message) => updates(message as SubscriptionResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SubscriptionResponse create() => SubscriptionResponse._();
  SubscriptionResponse createEmptyInstance() => create();
  static $pb.PbList<SubscriptionResponse> createRepeated() =>
      $pb.PbList<SubscriptionResponse>();
  @$core.pragma('dart2js:noInline')
  static SubscriptionResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<SubscriptionResponse>(create);
  static SubscriptionResponse _defaultInstance;

  @$pb.TagNumber(2)
  $core.String get ackInbox => $_getSZ(0);
  @$pb.TagNumber(2)
  set ackInbox($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasAckInbox() => $_has(0);
  @$pb.TagNumber(2)
  void clearAckInbox() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get error => $_getSZ(1);
  @$pb.TagNumber(3)
  set error($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasError() => $_has(1);
  @$pb.TagNumber(3)
  void clearError() => clearField(3);
}

class UnsubscribeRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('UnsubscribeRequest',
      package: const $pb.PackageName('pb'), createEmptyInstance: create)
    ..aOS(1, 'clientID', protoName: 'clientID')
    ..aOS(2, 'subject')
    ..aOS(3, 'inbox')
    ..aOS(4, 'durableName', protoName: 'durableName')
    ..hasRequiredFields = false;

  UnsubscribeRequest._() : super();
  factory UnsubscribeRequest() => create();
  factory UnsubscribeRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory UnsubscribeRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  UnsubscribeRequest clone() => UnsubscribeRequest()..mergeFromMessage(this);
  UnsubscribeRequest copyWith(void Function(UnsubscribeRequest) updates) =>
      super.copyWith((message) => updates(message as UnsubscribeRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UnsubscribeRequest create() => UnsubscribeRequest._();
  UnsubscribeRequest createEmptyInstance() => create();
  static $pb.PbList<UnsubscribeRequest> createRepeated() =>
      $pb.PbList<UnsubscribeRequest>();
  @$core.pragma('dart2js:noInline')
  static UnsubscribeRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<UnsubscribeRequest>(create);
  static UnsubscribeRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get clientID => $_getSZ(0);
  @$pb.TagNumber(1)
  set clientID($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasClientID() => $_has(0);
  @$pb.TagNumber(1)
  void clearClientID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get subject => $_getSZ(1);
  @$pb.TagNumber(2)
  set subject($core.String v) {
    $_setString(1, v);
  }

  @$pb.TagNumber(2)
  $core.bool hasSubject() => $_has(1);
  @$pb.TagNumber(2)
  void clearSubject() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get inbox => $_getSZ(2);
  @$pb.TagNumber(3)
  set inbox($core.String v) {
    $_setString(2, v);
  }

  @$pb.TagNumber(3)
  $core.bool hasInbox() => $_has(2);
  @$pb.TagNumber(3)
  void clearInbox() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get durableName => $_getSZ(3);
  @$pb.TagNumber(4)
  set durableName($core.String v) {
    $_setString(3, v);
  }

  @$pb.TagNumber(4)
  $core.bool hasDurableName() => $_has(3);
  @$pb.TagNumber(4)
  void clearDurableName() => clearField(4);
}

class CloseRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('CloseRequest',
      package: const $pb.PackageName('pb'), createEmptyInstance: create)
    ..aOS(1, 'clientID', protoName: 'clientID')
    ..hasRequiredFields = false;

  CloseRequest._() : super();
  factory CloseRequest() => create();
  factory CloseRequest.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory CloseRequest.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  CloseRequest clone() => CloseRequest()..mergeFromMessage(this);
  CloseRequest copyWith(void Function(CloseRequest) updates) =>
      super.copyWith((message) => updates(message as CloseRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CloseRequest create() => CloseRequest._();
  CloseRequest createEmptyInstance() => create();
  static $pb.PbList<CloseRequest> createRepeated() =>
      $pb.PbList<CloseRequest>();
  @$core.pragma('dart2js:noInline')
  static CloseRequest getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<CloseRequest>(create);
  static CloseRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get clientID => $_getSZ(0);
  @$pb.TagNumber(1)
  set clientID($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasClientID() => $_has(0);
  @$pb.TagNumber(1)
  void clearClientID() => clearField(1);
}

class CloseResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('CloseResponse',
      package: const $pb.PackageName('pb'), createEmptyInstance: create)
    ..aOS(1, 'error')
    ..hasRequiredFields = false;

  CloseResponse._() : super();
  factory CloseResponse() => create();
  factory CloseResponse.fromBuffer($core.List<$core.int> i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromBuffer(i, r);
  factory CloseResponse.fromJson($core.String i,
          [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) =>
      create()..mergeFromJson(i, r);
  CloseResponse clone() => CloseResponse()..mergeFromMessage(this);
  CloseResponse copyWith(void Function(CloseResponse) updates) =>
      super.copyWith((message) => updates(message as CloseResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CloseResponse create() => CloseResponse._();
  CloseResponse createEmptyInstance() => create();
  static $pb.PbList<CloseResponse> createRepeated() =>
      $pb.PbList<CloseResponse>();
  @$core.pragma('dart2js:noInline')
  static CloseResponse getDefault() => _defaultInstance ??=
      $pb.GeneratedMessage.$_defaultFor<CloseResponse>(create);
  static CloseResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get error => $_getSZ(0);
  @$pb.TagNumber(1)
  set error($core.String v) {
    $_setString(0, v);
  }

  @$pb.TagNumber(1)
  $core.bool hasError() => $_has(0);
  @$pb.TagNumber(1)
  void clearError() => clearField(1);
}
