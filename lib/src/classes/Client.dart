part of stan_dart;

class Client/* implements nats.Client*/ {
  final ProtocolHandler _protocolHandler;

  Client._(this._protocolHandler);

  static Future<Client> connectSingle(String host, String clientId, {int port, nats.ServerOptions serverOptions, nats.ClientOptions clientOptions, String username, String password, String clusterId, String discoveryPrefix}) {
    var natsClient = _natsClientFactory.connectSingle(host, port: port, serverOptions: serverOptions, clientOptions: clientOptions, username: username, password: password);

    return connectExisting(natsClient, clientId, clusterId: clusterId, discoveryPrefix: discoveryPrefix);
  }

  static Future<Client> connect(nats.ServerSet servers, String clientId, {nats.ServerOptions serverOptions, nats.ClientOptions clientOptions, String username, String password, String clusterId, String discoveryPrefix}) {
    var natsClient = _natsClientFactory.connect(servers, serverOptions: serverOptions, clientOptions: clientOptions, username: username, password: password);

    return connectExisting(natsClient, clientId, clusterId: clusterId, discoveryPrefix: discoveryPrefix);
  }

  static Future<Client> connectExisting(FutureOr<nats.Client> natsClient, String clientId, {String clusterId, String discoveryPrefix}) {
    return Future.value(natsClient).then((_natsClient) async {
      var protocolHandler = _protocolHandlerFactory.create(_natsClient, clientId, clusterId: clusterId, discoveryPrefix: discoveryPrefix);
      var stanClient      = Client._(protocolHandler);

      await stanClient._connect();

      return stanClient;
    });
  }

  Future<void> _connect() => _protocolHandler.connect();

  /// Closes the connection to the NATS server. A closed client cannot be reconnected.
  //void close() => _protocolHandler.close(); // TODO

  /// Publish a message to the given [subject], with optional [payload] and [encoding].
  /// [payload] will be encoded for transport using [encoding].
  /// If [payload] is already encoded, double-encoding can be avoided by explicitly setting [encoding] to null
  Future<void> publish(String subject, {dynamic payload, Encoding encoding = ascii}) async {
    assert(isNotBlank(subject), throw nats.NatsError.BAD_SUBJECT);

    await _protocolHandler.publish(subject, payload, encoding: encoding);
  }

  /// Subscribe to a given [subject]. Incoming messages are passed to the provided [handler].
  /// [maxInFlight] Maximum inflight messages without an acknowledgement allowed
  /// [ackWaitSeconds] Timeout for receiving an acknowledgement from the client
  /// [durableName] Optional durable name which survives client restarts
  /// [startPosition] An enumerated type specifying the point in history to start replaying data
  /// [startSequence] Optional start sequence number
  /// [startTimeDelta] Optional start time
  Future<Subscription> subscribe(String subject, {
      String         queue,
      int            maxInFlight    = ProtocolHandler.DEFAULT_MAX_INFLIGHT,
      int            ackWaitSeconds = ProtocolHandler.DEFAULT_SUBACK_SECONDS,
      String         durableName,
      StartPosition  startPosition,
      Int64          startSequence,
      Int64          startTimeDelta,
      MessageHandler handler
  }) async {
    assert(isNotBlank(subject), throw nats.NatsError.BAD_SUBJECT);

    Subscription subscription = await _protocolHandler.subscribe(subject,
      queue         : queue,
      maxInFlight   : maxInFlight,
      ackWaitSeconds: ackWaitSeconds,
      durableName   : durableName,
      startPosition : startPosition,
      startSequence : startSequence,
      startTimeDelta: startTimeDelta);

    if (handler != null) {
      subscription.stream.listen(handler);
    }

    return subscription;
  }

  /// Send a PING message to the server. Returns [true] if the server responds, or [false] if no response is received
  Future<bool> sendPing() => _protocolHandler.sendPing();

  // TODO: initially copied from nats.ts lib -- needs revisiting
  /// Flush outbound queue to server and call optional callback when server has processed all data.
  /// @param cb is optional, if not provided a Promise is returned. Flush is completed when promise resolves.
  /// @return Promise<void> or void if a callback was provided.
  /*FutureOr<void> flush([FlushCallback callback]) {
    return _protocolHandler.flush(callback);
  }*/

  // TODO: initially copied from nats.ts lib -- needs revisiting
  /// Drains all subscriptions. Returns a Promise that when resolved, indicates that all subscriptions have finished,
  /// and the client closed. Note that after calling drain, it is impossible to create new
  /// subscriptions or make any requests. As soon as all messages for the draining subscriptions are processed,
  /// it is also impossible to publish new messages.
  /// A drained connection is closed when the Promise resolves.
  /// @see [[Subscription.drain]]
  //Future<dynamic> drain() => _protocolHandler.drain();
}
