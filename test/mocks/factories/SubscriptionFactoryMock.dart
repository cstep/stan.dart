import 'package:nats_dart/nats_dart.dart' as nats;
import 'package:stan_dart/stan_dart.dart';
import 'package:mockito/mockito.dart';
import '../SubscriptionMocks.dart';

class SubscriptionFactoryMock extends Mock implements SubscriptionFactory {
  @override
  SubscriptionMock subscription(nats.Subscription delegate, String subject, String ackInbox, ProtocolHandler protocolHandler) {
    return SubscriptionMock({
      "_delegate"      : delegate,
      "subject"        : subject,
      "_ackInbox"       : ackInbox,
      "_protocolHandler": protocolHandler,
    });
  }

  @override
  DurableSubscriptionMock durableSubscription(nats.Subscription delegate, String subject, String durableName, String ackInbox, ProtocolHandler protocolHandler) {
    return DurableSubscriptionMock({
      "_delegate"      : delegate,
      "subject"        : subject,
      "durableName"    : durableName,
      "_ackInbox"       : ackInbox,
      "_protocolHandler": protocolHandler,
    });
  }
}
