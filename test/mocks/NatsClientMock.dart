import 'dart:convert';
import 'dart:mirrors';

import 'package:faker_extended/faker_extended.dart';
import 'package:mockito/mockito.dart';
import 'package:stan_dart/generated/stan/protocol.pb.dart';
import 'package:nats_dart/nats_dart.dart' as nats;
import 'package:protobuf/protobuf.dart';
import 'package:uuid/uuid.dart';
import '_ConstructorValueMock.dart';

class NatsClientMock extends ConstructorValueMock implements nats.Client {
  Map<String, dynamic> testMeta;
  @override nats.ClientOptions get options => values['clientOptions'];

  static Future<NatsClientMock> connect(nats.ServerSet servers, {nats.ServerOptions serverOptions, nats.ClientOptions clientOptions, String username, String password}) async {
    return NatsClientMock({'handler': null, 'clientOptions': clientOptions}, null);
  }

  static Future<NatsClientMock> connectSingle(String host, {int port, nats.ServerOptions serverOptions, nats.ClientOptions clientOptions, String username, String password}) async {
    return NatsClientMock({'handler': null, 'clientOptions': clientOptions}, null);
  }

  NatsClientMock(Map<String, dynamic> values, this.testMeta): super(values) {
    when(request(any, payload: anyNamed('payload'), encoding: anyNamed('encoding'), timeout: anyNamed('timeout'), onTimeout: anyNamed('onTimeout'))).thenAnswer((i) async {
      String callbackName;

      // ======== Determine Our Request Type
      var subject = i.positionalArguments.first;
      if (subject == '${testMeta['discoveryPrefix']}.${testMeta['clusterId']}') { // -- this is a connection request
        callbackName = '_mockConnectionResponse';

      } else if (subject.startsWith(testMeta['pubPrefix'] + '.')) {
        callbackName = '_mockPubAck';

      } else if (subject == testMeta['subRequests']) {
        callbackName = '_mockSubscriptionResponse';

      } else if (subject == testMeta['pingRequests']) {
        callbackName = '_mockPingResponse';

      } else {
        return null;
      }

      InstanceMirror selfReflection = reflect(this);
      var newI = Invocation.method(MirrorSystem.getSymbol(callbackName, selfReflection.type.owner), i.positionalArguments, i.namedArguments);

      return selfReflection.delegate(newI);
    });
  }

  nats.DataMessage _makeMockResponse(String subject, GeneratedMessage responseProto) {
    return nats.DataMessage(subject, Uuid().v4(), responseProto.writeToBuffer(), encoding: null);
  }

  nats.DataMessage _mockConnectionResponse(String subject, {dynamic payload, Encoding encoding, Duration timeout, void Function(Object error) onTimeout}) {
    testMeta.addAll({
      'pubPrefix'       : faker.lorem.word(),
      'subRequests'     : faker.lorem.word(),
      'unsubRequests'   : faker.lorem.word(),
      'closeRequests'   : faker.lorem.word(),
      'subCloseRequests': faker.lorem.word(),
      'pingRequests'    : faker.lorem.word(),
    });

    var request       = ConnectRequest.fromBuffer(payload);
    var responseProto = ConnectResponse()
      ..pubPrefix        = testMeta['pubPrefix']
      ..subRequests      = testMeta['subRequests']
      ..unsubRequests    = testMeta['unsubRequests']
      ..closeRequests    = testMeta['closeRequests']
      ..subCloseRequests = testMeta['subCloseRequests']
      ..pingRequests     = testMeta['pingRequests']
      ..pingInterval     = request.pingInterval
      ..pingMaxOut       = request.pingMaxOut
      ..protocol         = request.protocol;

    return _makeMockResponse(subject, responseProto);
  }

  nats.DataMessage _mockPingResponse(String subject, {dynamic payload, Encoding encoding, Duration timeout, void Function(Object error) onTimeout}) {
    var responseProto = PingResponse();

    return _makeMockResponse(subject, responseProto);
  }

  nats.DataMessage _mockPubAck(String subject, {dynamic payload, Encoding encoding, Duration timeout, void Function(Object error) onTimeout}) {
    var responseProto = PubAck()
      ..guid = Uuid().v4();

    return _makeMockResponse(subject, responseProto);
  }

  nats.DataMessage _mockSubscriptionResponse(String subject, {dynamic payload, Encoding encoding, Duration timeout, void Function(Object error) onTimeout}) {
    var responseProto = SubscriptionResponse()
      ..ackInbox = faker.lorem.word();

    return _makeMockResponse(subject, responseProto);
  }
}
