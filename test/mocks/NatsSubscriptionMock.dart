import 'dart:async';
import 'package:faker_extended/faker_extended.dart';
import 'package:stan_dart/generated/stan/protocol.pb.dart';
import 'package:nats_dart/nats_dart.dart' as nats;
import '_ConstructorValueMock.dart';

class NatsSubscriptionMock extends ConstructorValueMock implements nats.Subscription {
  @override
  Stream<nats.DataMessage>           get  stream => _streamController.stream;
  StreamController<nats.DataMessage>     _streamController = StreamController<nats.DataMessage>();

  NatsSubscriptionMock(String subject, {String queue}): super({'subject': subject, 'queue': queue});

  void fakeIncoming(List<int> encodedPayload) {
    var proto = MsgProto()
      ..data    = encodedPayload
      ..subject = subject;

    var message = nats.DataMessage(this.subject, faker.lorem.word(), proto.writeToBuffer(), encoding: null);
    _streamController.add(message);
  }
}
