import 'dart:async';
import 'dart:convert';
import 'package:faker_extended/faker_extended.dart';
import 'package:stan_dart/generated/stan/protocol.pb.dart';
import 'package:stan_dart/stan_dart.dart';
import '_ConstructorValueMock.dart';

abstract class SubscriptionMockBase extends ConstructorValueMock implements Subscription {
  final StreamController<DataMessage> _streamController = StreamController<DataMessage>();

  @override
  Stream<DataMessage> get stream => _streamController.stream;

  @override String get inbox => values['_delegate'].subject;
  @override String get queue => values['_delegate'].queue;

  SubscriptionMockBase(Map<String, dynamic> values) : super(values);

  void simulateIncomingDataMessage({String payload, Encoding encoding = ascii}) {
    //if (_isClosed) return; // -- early
    payload     ??= faker.lorem.sentences(faker.randomGenerator.integer(10)).join(' ');
    var subject   = faker.lorem.word();
    var proto     = MsgProto()
      ..data    = (encoding == null ? payload : encoding.encode(payload))
      ..subject = subject;

    _streamController.add(DataMessage.fromProto(proto, encoding: encoding));
  }

//  @override
//  dynamic noSuchMethod(Invocation invocation) {
//    if (invocation.memberName == #_stream) {
//      // _stream is normally private and as such, when using this mock, wont be accessible from inside our package
//      return this.stream; // proxy our mock stream
//    } else {
//      return super.noSuchMethod(invocation);
//    }
//  }
}

class SubscriptionMock        extends SubscriptionMockBase implements Subscription        {        SubscriptionMock(Map<String, dynamic> values) : super(values); }
class DurableSubscriptionMock extends SubscriptionMock     implements DurableSubscription { DurableSubscriptionMock(Map<String, dynamic> values) : super(values); }
