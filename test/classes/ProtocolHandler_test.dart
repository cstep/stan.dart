import 'dart:convert';
import 'dart:mirrors';
import 'package:mockito/mockito.dart';
import 'package:stan_dart/generated/stan/protocol.pb.dart';
import 'package:stan_dart/stan_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';
import '../helpers/VerificationHelper.dart';
import '../helpers/matchers.dart';
import '../mocks/DataMessageMock.dart';
import '../mocks/NatsClientMock.dart';
import '../mocks/NatsSubscriptionMock.dart';
import '../mocks/SubscriptionMocks.dart';
import '../mocks/factories/NatsClientFactoryMock.dart';
import '../mocks/factories/SubscriptionFactoryMock.dart';

void main() {
  container
    ..unregister<NatsClientFactory  >()..registerInstance<NatsClientFactory  >(NatsClientFactoryMock())
    ..unregister<SubscriptionFactory>()..registerInstance<SubscriptionFactory>(SubscriptionFactoryMock());

  NatsClientMock       underlyingNatsClient;
  ProtocolHandler      handler;
  Map<String, dynamic> expectedProperties;

  void verifyUnderlyingRequest(VerificationResult verification, Map<String, dynamic> expectedArgs) {
    var verificationHelper = VerificationHelper({
      'subject' : 0,
      'payload' : 1,
      'encoding': 2,
    });

    verificationHelper.verifyResult(verification,
      expectedCallCount: 1,
      expectedArgs     : expectedArgs,
      reasonPrefix     : 'Underlying Nats Client `request()` method'
    );
  }

  setUp(() {
    String clientId        = faker.lorem.word();
    String clusterId       = faker.lorem.word();
    String discoveryPrefix = faker.lorem.word();

    Map<String, dynamic> testMeta = {
      'clientId'        : clientId,
      'clusterId'       : clusterId,
      'discoveryPrefix' : discoveryPrefix,
    };
    underlyingNatsClient = (container.resolve<NatsClientFactory>() as NatsClientFactoryMock).create(null, null, testMeta);

    expectedProperties = {
      '_natsClient'     : underlyingNatsClient,
      'clientId'        : clientId,
      'clusterId'       : clusterId,
      'discoveryPrefix' : discoveryPrefix,
      '_heartbeat'      : isNotNull,
      'connectionId'    : isNotNull,
    };

    handler = ProtocolHandler(expectedProperties['_natsClient'], expectedProperties['clientId'],
      clusterId      : expectedProperties['clusterId'],
      discoveryPrefix: expectedProperties['discoveryPrefix']
    );
  });

  tearDown(() {
    reflect(handler).getField(MirrorSystem.getSymbol('_heartbeat', reflect(handler).type.owner)).reflectee.cancel(); // -- close futures/timers that will hold the script running
  });

  test('can be created', () {
    var handlerMirror = reflect(handler);
    expectedProperties.forEach((key, expectedValue) {
      var actualValue = handlerMirror.getField(MirrorSystem.getSymbol(key, handlerMirror.type.owner)).reflectee;
      Matcher matcher = equals(expectedValue);
      if (expectedValue is Matcher) {
        matcher = expectedValue;
      }

      expect(actualValue, matcher, reason: 'ProtocolHandler value for `$key` other than expected');
    });
  });

  test('can connect', () async {
    // ======== Connect
    await handler.connect();

    // ======== Verify
    var verification = verify(underlyingNatsClient.request(captureAny, payload: captureAnyNamed('payload'), encoding: captureAnyNamed('encoding')));

    var expectedArgs = {
      'subject' : '${handler.discoveryPrefix}.${handler.clusterId}',
      'encoding': null,
      'payload' : predicate((protoBuffer) {
        var protoMessage = ConnectRequest.fromBuffer(protoBuffer);
        return isA<ConnectRequest>()
            .having((x) => x.clientID,       'clientId',       equals(handler.clientId))
            .having((x) => x.heartbeatInbox, 'heartbeatInbox', equals(handler.connectionId))
            .having((x) => x.connID,         'connectionId',   equals(reflect(handler).getField(MirrorSystem.getSymbol('_connectionId', reflect(handler).type.owner)).reflectee))
            .having((x) => x.protocol,       'protocol',       equals(1))
            .having((x) => x.pingInterval,   'pingInterval',   equals(underlyingNatsClient.options.pingInterval.inSeconds))
            .having((x) => x.pingMaxOut,     'pingMaxOut',     equals(underlyingNatsClient.options.maxPingsOutstanding))
          .matches(protoMessage, {});
      })
    };

    verifyUnderlyingRequest(verification, expectedArgs);
  });

  //@depends('can connect');
  group('can send commands', () {
    ConnectResponse clientConfig;

    setUp(() {
      return handler.connect()
        .then((_) {
          clientConfig = reflect(handler).getField(MirrorSystem.getSymbol('_clientConfig', reflectClass(ProtocolHandler).owner)).reflectee;
          clearInteractions(underlyingNatsClient);
        });
    });

    test('sendPing()', () { // -- no optional arguments
      handler.sendPing();

      // ======== Verify
      var verification = verify(underlyingNatsClient.request(captureAny, payload: captureAnyNamed('payload'), encoding: captureAnyNamed('encoding')));

      var expectedArgs = {
        'subject' : clientConfig.pingRequests,
        'encoding': null,
        'payload' : transformed((protoBuffer) => Ping.fromBuffer(protoBuffer),
          isA<Ping>()
            .having((x) => x.connID, 'connectionId', equals(reflect(handler).getField(MirrorSystem.getSymbol('_connectionId', reflect(handler).type.owner)).reflectee))
        )
      };

      verifyUnderlyingRequest(verification, expectedArgs);
    });

    test('publish()', () {
      var expectedSubject  = faker.lorem.word();
      var expectedPayload  = faker.lorem.sentences(faker.random.int(max: 10, min: 1)).join(' ');
      var expectedEncoding = faker.random.element([ascii, utf8, latin1]);
      handler.publish(expectedSubject, expectedPayload, encoding: expectedEncoding);

      // ======== Verify
      var verification = verify(underlyingNatsClient.request(captureAny, payload: captureAnyNamed('payload'), encoding: captureAnyNamed('encoding')));

      var expectedArgs = {
        'subject' : '${clientConfig.pubPrefix}.${expectedSubject}',
        'encoding': null,
        'payload' : transformed((protoBuffer) => PubMsg.fromBuffer(protoBuffer),
          isA<PubMsg>()
            .having((x) => x.clientID, 'clientID',     equals(handler.clientId))
            .having((x) => x.guid,     'guid',         isNotNull)
            .having((x) => x.subject,  'subject',      equals(expectedSubject))
            .having((x) => x.data,     'data',         equals(expectedEncoding.encode(expectedPayload)))
            .having((x) => x.connID,   'connectionId', equals(reflect(handler).getField(MirrorSystem.getSymbol('_connectionId', reflect(handler).type.owner)).reflectee))
        )
      };

      verifyUnderlyingRequest(verification, expectedArgs);
    });

    test('subscribe()', () async {
      var inputs = {
        'subject'       : faker.lorem.word(),
        'queue'         : faker.lorem.word(),
        'maxInFlight'   : faker.random.int(min: 1, max: 10),
        'ackWaitSeconds': faker.random.int(min: 1, max: 10),
        'durableName'   : faker.lorem.word(),
        'startPosition' : faker.random.element(StartPosition.values),
        'startSequence' : faker.random.int64(),
        'startTimeDelta': faker.random.int64(),
      };

      var actual = await handler.subscribe(inputs['subject'],
        queue         : inputs['queue'],
        maxInFlight   : inputs['maxInFlight'],
        ackWaitSeconds: inputs['ackWaitSeconds'],
        durableName   : inputs['durableName'],
        startPosition : inputs['startPosition'],
        startSequence : inputs['startSequence'],
        startTimeDelta: inputs['startTimeDelta'],
      );

      // ======== Verify
      // -------- end-result
      expect(actual,         isA<Subscription>(),       reason: 'Returned subscription other than expected');
      expect(actual.subject, equals(inputs['subject']), reason: 'Returned subscription subject other than expected');

      var _ackInbox = reflect(actual).getField(MirrorSystem.getSymbol('_ackInbox', reflect(actual).type.owner)).reflectee;
      expect(_ackInbox, isNotNull, reason: 'Returned subscription ackInbox unexpectedly not set');

      var _handler = reflect(actual).getField(MirrorSystem.getSymbol('_protocolHandler', reflect(actual).type.owner)).reflectee;
      expect(_handler, same(handler), reason: 'Returned subscription references unexpected protocolHandler instance');

      // -------- underlying request
      var verification = verify(underlyingNatsClient.request(captureAny, payload: captureAnyNamed('payload'), encoding: captureAnyNamed('encoding')));

      var expectedArgs = {
        'subject' : '${clientConfig.subRequests}',
        'encoding': null,
        'payload' : transformed<List<int>, SubscriptionRequest>((protoBuffer) =>
          SubscriptionRequest.fromBuffer(protoBuffer),
          isA<SubscriptionRequest>()
            .having((x) => x.clientID,       'clientID',       equals(handler.clientId))
            .having((x) => x.subject,        'subject',        equals(inputs['subject']))
            .having((x) => x.inbox,          'inbox',          startsWith("${inputs['subject']}_${inputs['queue']}_"))
            .having((x) => x.qGroup,         'queue',          equals(inputs['queue']))
            .having((x) => x.maxInFlight,    'maxInFlight',    equals(inputs['maxInFlight']))
            .having((x) => x.ackWaitInSecs,  'ackWaitInSecs',  equals(inputs['ackWaitSeconds']))
            .having((x) => x.durableName,    'durableName',    equals(inputs['durableName']))
            .having((x) => x.startPosition,  'startPosition',  equals(inputs['startPosition']))
            .having((x) => x.startSequence,  'startSequence',  equals(inputs['startSequence']))
            .having((x) => x.startTimeDelta, 'startTimeDelta', equals(inputs['startTimeDelta']))
        )
      };

      verifyUnderlyingRequest(verification, expectedArgs);
    });

    test('unsubscribe()', () {
      var subscribeInputs = {
        'subject'    : faker.lorem.word(),
        'inbox'      : faker.lorem.word(),
        'durableName': faker.lorem.word(),
      };

      var underlyingNatsSubscription = NatsSubscriptionMock(subscribeInputs['inbox']);
      var subscription               = container.resolve<SubscriptionFactory>().durableSubscription(underlyingNatsSubscription, subscribeInputs['subject'], subscribeInputs['durableName'], faker.lorem.word(), handler);

      handler.unsubscribe(subscription);

      // ======== Verify
      var verification = verify(underlyingNatsClient.publish(captureAny, payload: captureAnyNamed('payload'), encoding: captureAnyNamed('encoding')));

      var expectedArgs = {
        'subject' : '${clientConfig.unsubRequests}',
        'encoding': null,
        'payload' : transformed<List<int>, UnsubscribeRequest>((protoBuffer) =>
          UnsubscribeRequest.fromBuffer(protoBuffer),
          isA<UnsubscribeRequest>()
            .having((x) => x.clientID,    'clientID',    equals(handler.clientId))
            .having((x) => x.subject,     'subject',     equals(subscribeInputs['subject']))
            .having((x) => x.inbox,       'inbox',       equals(subscribeInputs['inbox']))
            .having((x) => x.durableName, 'durableName', equals(subscribeInputs['durableName']))
        )
      };

      verifyUnderlyingRequest(verification, expectedArgs);
    });

    test('closeSubscription()', () {
      var subscribeInputs = {
        'subject'    : faker.lorem.word(),
        'inbox'      : faker.lorem.word(),
        'durableName': faker.lorem.word(),
      };

      var underlyingNatsSubscription = NatsSubscriptionMock(subscribeInputs['inbox']);
      var subscription               = container.resolve<SubscriptionFactory>().durableSubscription(underlyingNatsSubscription, subscribeInputs['subject'], subscribeInputs['durableName'], faker.lorem.word(), handler);

      handler.closeSubscription(subscription);

      // ======== Verify
      var verification = verify(underlyingNatsClient.publish(captureAny, payload: captureAnyNamed('payload'), encoding: captureAnyNamed('encoding')));

      var expectedArgs = {
        'subject' : '${clientConfig.closeRequests}',
        'encoding': null,
        'payload' : transformed<List<int>, UnsubscribeRequest>((protoBuffer) =>
          UnsubscribeRequest.fromBuffer(protoBuffer),
          isA<UnsubscribeRequest>()
            .having((x) => x.clientID,    'clientID',    equals(handler.clientId))
            .having((x) => x.subject,     'subject',     equals(subscribeInputs['subject']))
            .having((x) => x.inbox,       'inbox',       equals(subscribeInputs['inbox']))
            .having((x) => x.durableName, 'durableName', equals(subscribeInputs['durableName']))
        )
      };

      verifyUnderlyingRequest(verification, expectedArgs);
    });

    test('acknowledge()', () {
      var subscribeInputs = {
        'subject'    : faker.lorem.word(),
        'inbox'      : faker.lorem.word(),
        'durableName': faker.lorem.word(),
      };

      var underlyingNatsSubscription = NatsSubscriptionMock(subscribeInputs['inbox']);
      DurableSubscriptionMock subscription = container.resolve<SubscriptionFactory>().durableSubscription(underlyingNatsSubscription, subscribeInputs['subject'], subscribeInputs['durableName'], faker.lorem.word(), handler);

      var messageInputs = {
        'subject'      : subscribeInputs['subject'],
        'sequence'     : faker.random.int64(),
        'timestamp'    : faker.random.int64(),
        'isRedelivered': faker.random.boolean(),
      };

      var proto = MsgProto()
        ..subject     = messageInputs['subject']
        ..sequence    = messageInputs['sequence']
        ..timestamp   = messageInputs['timestamp']
        ..redelivered = messageInputs['isRedelivered'];
      var dataMessage = DataMessageMock({'_proto': proto, 'encoding': messageInputs['encoding']});

      handler.acknowledge(subscription, dataMessage);

      // ======== Verify
      var verification = verify(underlyingNatsClient.publish(captureAny, payload: captureAnyNamed('payload'), encoding: captureAnyNamed('encoding')));

      var expectedArgs = {
        'subject' : reflect(subscription).getField(MirrorSystem.getSymbol('_ackInbox', reflect(subscription).type.owner)).reflectee,
        'encoding': null,
        'payload' : transformed<List<int>, Ack>((protoBuffer) =>
          Ack.fromBuffer(protoBuffer),
          isA<Ack>()
            .having((x) => x.subject,  'subject',  equals(messageInputs['subject']))
            .having((x) => x.sequence, 'sequence', equals(messageInputs['sequence']))
        )
      };

      verifyUnderlyingRequest(verification, expectedArgs);
    });
  });
}
