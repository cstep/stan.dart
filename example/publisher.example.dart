import 'dart:async';
import 'dart:io';
import 'package:nats_dart/nats_dart.dart' show log;
import 'package:stan_dart/stan_dart.dart' as stan;
import './helpers/example_helpers.dart';

int    maxMessages   = 0; // -- 0 = infinite
String stanClusterId = Platform.environment['STAN_CLUSTER_ID']??'default';

void _main() {
  log.general.config('starting up...');

  withProducerColorSet((client) async { // -- this wrapper is just to help with colorizing our output
    var streamingClient = await stan.Client.connectExisting(client, 'producer', clusterId: stanClusterId); // -- connect to stan with our already-connected nats client

    var counter = 0;
    Timer.periodic(Duration(seconds: 1), (timer) {
      streamingClient.publish('myTopic', payload: 'this is an event (#${++counter}) on topic: myTopic');
      if ((maxMessages??0) > 0 && counter >= maxMessages) {
        timer.cancel();
      }
    }); // -- encode it ourselves and tell the client not to worry about it
  });
}

void main() {
  setupLogger();
  _main();
}
